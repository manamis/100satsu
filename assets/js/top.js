
$(function(){

// ----------------------------------------------------------------------------
// Opening Slide Show
// ----------------------------------------------------------------------------
	var openingSlideShow = function(){

		var options = {
			easing   : 'easeInQuad',
			interval : 5000
		};

		var $container    = $('#container'),
			$header       = $('#header'),
			$content      = $('#content'),
			$footer       = $('#footer'),
			$qunta        = $('#qunta'),
			$sound        = $('.sound-select'),
			$soundOn      = $sound.find('.on'),
			$soundOff     = $sound.find('.off'),
			$enter        = $('.enter'),
			$enterImg     = $('.enter').find('img'),
			/*$page1        = $('.page-01'),
			$page2        = $('.page-02'),
			$page3        = $('.page-03'),
			$page4        = $('.page-04'),
			$page5        = $('.page-05'),
			$page6        = $('.page-06'),*/
			$loader       = $('.loader'),
			soundFlg      = false;
			firstSoundFlg = false;
			SPSoundFlg    = false;

		var init = function(){

			$('html,body').animate({ scrollTop: 0 }, '100', function(){
				$content.hide();
			});
			$('.sound-select, .enter, #header, #content, #footer, #qunta').css({ opacity : 0 });
			imgPreload();

		}

		var imgPreload = function(){
			$container.imagesLoaded(function(){
				$.cookie("shincho-2015-access", 'access', { expires: date, path: '/' });
				$loader.delay(1000).fadeOut(1500, function(){
					//$sound.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },500, 'linear');
					$enter.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },500, 'linear');
					eventSet();
				});
			});
		}

		var eventSet = function(){
			$soundOn.on('click', function(){ $soundOnClick($(this)) });
			$soundOff.on('click', function(){ $soundOffClick($(this)) });
			$enterImg.on('click', function(){ $enterClick($(this)) });
		}

		var $soundOnClick = function($elm){
			$sound.stop().fadeOut(500, 'linear');
			$enter.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },500, 'linear');
			document.getElementById('click-sound').play();
			SoundStart();
			soundFlg = true;
			SPSoundFlg = true;
			firstSoundFlg = false;
			$('.sound-check').addClass('on');
			$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-on_02.png');
			$.cookie('shincho-sound', 'sound', { expires: date, path: '/' });
			$.cookie("shincho-2015-access", 'access', { expires: date, path: '/' });
		}

		var $soundOffClick = function($elm){
			$sound.stop().fadeOut(500, 'linear');
			$enter.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },500, 'linear');
			soundFlg = false;
			$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-off_02.png');
			SoundOFFCheck();
			$.cookie("shincho-2015-access", 'access', { expires: date, path: '/' });
		}

		var $enterClick = function($elm){
			if( ua.uaObj.uaDevice === 'device-mobile' || ua.uaObj.uaDevice === 'ipad' ){
				if( SPSoundFlg === true ){
					document.getElementById('bgm-sound-sp').play();
				}
			}

				$enter.stop().animate({ 'opacity' : 0 },600, 'linear', function(){
					$enter.hide();
				});

				if( ua.uaObj.uaDevice !== 'device-mobile' && ua.uaObj.uaDevice !== 'ipad' ){
					if( soundFlg === true ){
						document.getElementById('bgm-sound').play();
					}
				}

				$('body').css('background-image', 'url(/assets/img/bg/bg_body.png)');
				$header.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
				$content.css({ visibility : 'visible', display : 'block' }).stop().animate({ 'opacity' : 1 },600, 'linear');
				$qunta.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
				$footer.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
				$('#nav-pagetop, #nav-pagetop-sp').css({ opacity : 1 });
			
			//$enter.hide();
//			$page1.css({ visibility : 'visible', display : 'block' });

			//setTimeout(function(){
//				$page1.hide();
//				$page2.css({ visibility : 'visible', display : 'block' });
//			}, 500);
//
//			setTimeout(function(){
//				$page2.hide();
//				$page3.css({ visibility : 'visible', display : 'block' });
//			}, 1000);
//
//			setTimeout(function(){
//				$page3.hide();
//				$page4.css({ visibility : 'visible', display : 'block' });
//			}, 1500);
//
//			setTimeout(function(){
//				$page4.hide();
//				$page5.css({ visibility : 'visible', display : 'block' });
//			}, 2000);
//
//			setTimeout(function(){
//				$page5.hide();
//				$page6.css({ visibility : 'visible', display : 'block' });
//			}, 2500);
//
//			setTimeout(function(){
//				$page6.stop().animate({ 'opacity' : 0 },600, 'linear', function(){
//					$page6.hide();
//				});
//
//				if( ua.uaObj.uaDevice !== 'device-mobile' && ua.uaObj.uaDevice !== 'ipad' ){
//					if( soundFlg === true ){
//						document.getElementById('bgm-sound').play();
//					}
//				}
//
//				$('body').css('background-image', 'url(/assets/img/bg/bg_body.png)');
//				$header.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
//				$content.css({ visibility : 'visible', display : 'block' }).stop().animate({ 'opacity' : 1 },600, 'linear');
//				$qunta.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
//				$footer.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
//				$('#nav-pagetop, #nav-pagetop-sp').css({ opacity : 1 });
//			}, 4000);
		}

		init();
	}

// ----------------------------------------------------------------------------
// Sound
// ----------------------------------------------------------------------------
	SoundStart = function(){
		var $soundCheck  = $('.sound-check'),
			$soundClick  = $('.sound-click'),
			bgm           = document.getElementById('bgm-sound'),
			bgmSP         = document.getElementById('bgm-sound-sp'),
			clickSound    = document.getElementById('click-sound'),
			checkflg      = true;
			ClickSoundFlg = true;

		$soundCheck.on('click', function(){ $soundCheckClick($(this)) });

		var $soundCheckClick = function($elm){
			if( checkflg === true ){
				checkflg = false;
				firstSoundFlg = false;

				if( $elm.hasClass('on') ){
					$elm.removeClass('on').find('img').attr('src', '/assets/img/btn/btn-sound-off_02.png');
					ClickSoundFlg = false;
					$.removeCookie('shincho-sound', { expires: date, path: '/'});
					bgm.pause();
					bgmSP.pause();
					//bgm.currentTime = 0;
				} else{
					$elm.addClass('on').find('img').attr('src', '/assets/img/btn/btn-sound-on_02.png');
					ClickSoundFlg = true;
					$.cookie('shincho-sound', 'sound', { expires: date, path: '/' });
					bgm.play();
					clickSound.play();
				}

				setTimeout(function(){
					checkflg = true;
				}, 300);
			}
		}

		$soundClick.on('click', function(e){
			if( ClickSoundFlg === true ){

				if( ua.uaObj.uaDevice === 'device-mobile' ){
					clickSound.pause();
					clickSound.currentTime = 0;
					clickSound.play();
				} else{
					e.preventDefault();

					var href = $(this).attr('href');

					clickSound.pause();
					clickSound.currentTime = 0;
					clickSound.play();

					if( href ){
						if( $(this).hasClass('window-open') ){
							setTimeout(function(){
								window.open().location.href = href;
							}, 1000);
						} else if( $(this).hasClass('no-window') ){
							return false;
						} else{
							setTimeout(function(){
								location.href = href;
							}, 1000);
						}
					}
				}
			}
		});
	}

	var SoundOFFCheck = function(){
		$('.sound-check').on('click', function(){
			if( firstSoundFlg === true ){
				firstSoundFlg = false;
				$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-on_02.png');
				$('.sound-check').addClass('on');
				SoundStart();
				document.getElementById('bgm-sound').play();
				$.cookie('shincho-sound', 'sound', { expires: date, path: '/' });
			}
		});
	}


// ----------------------------------------------------------------------------
// More Button Click
// ----------------------------------------------------------------------------
	var moreClick = function(){
		var $more   = $('.nav-more-01'),
			m_flg_01 = true,
			m_flg_02 = true;

		var list1 = [ '.item-43', '.item-44', '.item-45', '.movie-5', '.item-46', '.item-47', '.item-48', '.item-49', '.item-50', '.item-51', '.item-52', '.item-53', '.item-54', '.movie-6', '.item-55', '.item-56', '.item-57', '.item-58', '.item-59', '.item-60', '.item-61', '.item-62', '.item-63', '.item-64', '.item-65', '.item-66', '.movie-7', '.item-67', '.item-68', '.item-69', '.item-70', '.item-71', '.item-72', '.item-73', '.item-74', '.item-75', '.movie-8', '.item-76', '.item-77', '.item-78', '.item-79', '.item-80', '.item-81', '.item-82', '.item-83', '.item-84' ];

		var list2 = ['.item-85', '.item-86', '.item-87', '.movie-9', '.item-88', '.item-89', '.item-90', '.item-91', '.item-92', '.item-93', '.item-94', '.item-95', '.item-96', '.movie-10', '.item-97', '.item-98', '.item-99', '.item-100', '.item-101', '.item-102', '.item-103', '.item-104', '.item-105', '.item-106', '.item-107', '.item-108', '.item-109', '.item-110', '.item-111' ];

		$more.on('click', function(){
			if( m_flg_01 === true ){
				$('.list-index-01-container').addClass('display-2-height').removeClass('display-1-height');

				for (i = 0; i < list1.length; i++) {
					$(list1[i]).delay(i*(150)).css({display:'block',opacity:'0'}).animate({opacity:'1'},800, 'swing');
				}

				m_flg_01 = false;
				return false;
			} else if( m_flg_02 === true ){
				$('.list-index-01-container').removeClass('display-2-height');
				$(this).hide();

				for (m = 0; m < list2.length; m++) {
					$(list2[m]).delay(m*(150)).css({display:'block',opacity:'0'}).animate({opacity:'1'},800, 'swing');
				}
			}
		});
	}


// ----------------------------------------------------------------------------
// DOM Set
// ----------------------------------------------------------------------------
	var setDOM = function(data){
		var $listIndex = $('.list-index-01'),
			$dataLen   = 0,
			category   = '',
			num        = [],
			numShuffle = [];

		if( ua.uaObj.uaBrouser !== 'ie8' ){
			$dataLen   = Object.keys(data).length;
		} else{
			$dataLen   = 107;
			$('.sound-check').hide();
			$.cookie('shincho-sound', 'sound', { expires: date, path: '/' });
			$.cookie("shincho-2015-access", 'access', { expires: date, path: '/' });
		}

		$.each( data, function(i){

			switch ( data[i]['category'] ){
				case '豕｣縺代ｋ譛ｬ' :
					category = 'cry';
				break;
				case '繝､繝舌う譛ｬ' :
					category = 'dangerous';
				break;
				case '諱九☆繧区悽' :
					category = 'love';
				break;
				case '閠・∴繧区悽' :
					category = 'think';
				break;
				case '繧ｷ繝薙ｌ繧区悽' :
					category = 'becomenumb';
				break;
			}

			var listHTML   = '<li><a href="/detail/index.html?book'+ data[i]['id'] +'?'+ category +'"><img src="' + data[i]['image'] +'" alt=""></a></li>';

			$listIndex.append(listHTML);

			if( $dataLen === i+1 ){
				if( !$.cookie("shincho-2015-access") && ua.uaObj.uaBrouser !== 'ie8' ){
					openingSlideShow();
				} else{
					$('.lyt-opening-01').hide();
					$('body').css('background-image', 'url(/assets/img/bg/bg_body.png)');
					$('#header').css({ visibility : 'visible', display : 'block' });
					$('#content').css({ visibility : 'visible' });
					$('#footer').css({ visibility : 'visible' });
					$('#qunta').css({ visibility : 'visible' });
					$('#nav-pagetop, #nav-pagetop-sp').css({ opacity : 1 });

					if( ua.uaObj.uaBrouser !== 'ie8' ){
						if( $.cookie('shincho-sound') ){
							$('.sound-check').addClass('on');
							$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-on_02.png');
							document.getElementById('bgm-sound').play();
							SoundStart();
						} else{
							var soundCheckFirstFlag = true;
							$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-off_02.png');
							$('.sound-check').on('click', function(){
								if( soundCheckFirstFlag === true ){
									soundCheckFirstFlag = false;
									$('.sound-check').addClass('on');
									$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-on_02.png');
									document.getElementById('bgm-sound').play();
									SoundStart();
									$.cookie('shincho-sound', 'sound', { expires: date, path: '/' });
								}
							});
						}
					}
				}

				for( var i=1; i<=$listIndex.find('li').length; i++ ){
					num.push(i);
				}

				Array.prototype.shuffle = function() {
					var i = this.length;
					while(i){
						var j = Math.floor(Math.random()*i);
						var t = this[--i];
						this[i] = this[j];
						this[j] = t;
					}
					return this;
				}
				numShuffle = num.shuffle();

				// 100蜀贋ｸ隕ｧ縺ｮ譛ｬ繧偵Λ繝ｳ繝繝驟咲ｽｮ
				$listIndex.find('li').each(function(i){
					$(this).addClass('item-' + numShuffle[i] );
				});

				if( ua.uaObj.uaDevice !== 'device-mobile' && ua.uaObj.uaDevice !== 'ipad' ){
					PlayMarkHover();
				}

				moreClick();
			};
		});
	}


	var PlayMarkHover = function(){
		$('.movie').find('a').on('mouseenter', function(){
			var $img = $(this).find('.play').find('img');
			$img.attr('src',$img.attr('src').replace('.png','_o.png'));
		});

		$('.movie').find('a').on('mouseleave', function(){
			var $img = $(this).find('.play').find('img');
			$img.attr('src',$img.attr('src').replace('_o.png','.png'));
		});
	}


	var setmovieDOM = function(data){
		var $listIndex = $('.list-index-01');

		$.each( data, function(i){
			//var movieHTML   = '<p class="movie movie-'+ (i+1) +'"><a href="'+ data[i]['url'] +'"><img src="' + data[i]['image'] + '" alt=""><span class="play"><img src="/assets/img/btn/btn_play_01.png" alt=""></span></a></p>';
			var movieHTML   = '<p class="movie movie-'+ (i+1) +'"><img src="' + data[i]['image'] + '" alt=""></p>';
			$listIndex.after(movieHTML);
		});
	}


// ----------------------------------------------------------------------------
// JSON Data Load
// ----------------------------------------------------------------------------
	var getJSONData = function(path,type){
		var dfd=$.Deferred();
		$.ajax({
			cache:false,
			url:path,
			type:type,
			data:{},
			dataType:"json",
			success:function(data){
				dfd.resolve(data);
			},
			error:function(data){
				dfd.reject(data);
			}
		});
		return dfd.promise();
	}

	$(window).load(function(){
/*
		getJSONData('/api/s_book100.php', 'GET').done(
*/
		getJSONData('/api/s_book100.json', 'GET').done(
			function(data){
				setDOM(data);
			}
		);
/*
		getJSONData('/api/s_movie100.php', 'GET').done(
*/
		getJSONData('/api/s_movie100.json', 'GET').done(
			function(data){
				setmovieDOM(data);
			}
		);
	});

});