$(function(){

// ----------------------------------------------------------------------------
// Jenre JS
// ----------------------------------------------------------------------------
	var setDOM = function(data){
		var $listIndex  = $('.list-jenre-01'),
			$hdg        = $('.hdg-level2-02'),
			$hdgAlt     = '',
			$footer     = $('#footer'),
			$qunta      = $('#qunta'),
			categoryURL = location.href.split('?')[1],
			num          = [],
			numShuffle   = [];
			category    = '';

		if(! categoryURL){
			location.href = "/jenre/";
		}

		switch ( categoryURL ){
			case 'cry' : $hdgAlt = '泣ける本'
			break;
			case 'dangerous' : $hdgAlt = 'ヤバイ本'
			break;
			case 'love' : $hdgAlt = '恋する本'
			break;
			case 'think' : $hdgAlt = '考える本'
			break;
			case 'becomenumb' : $hdgAlt = 'シビれる本'
			break;
		}

		$listIndex.addClass(categoryURL );

		$hdg.append('<img src="/assets/img/hdg/hdg_' + categoryURL + '_01.png" alt="'+ $hdgAlt +'">');

		$('#qunta').find('img').attr('src', '/assets/img/img/img_qunta_01_' + categoryURL + '.gif').css({ visibility : 'visible' });

		setTimeout(function(){
			$footer.addClass(categoryURL).css({ visibility : 'visible' });
		}, 600);

		$.each( data, function(i){

			switch ( data[i]['category'] ){
				case '泣ける本' :
					category = 'cry';
				break;
				case 'ヤバイ本' :
					category = 'dangerous';
				break;
				case '恋する本' :
					category = 'love';
				break;
				case '考える本' :
					category = 'think';
				break;
				case 'シビれる本' :
					category = 'becomenumb';
				break;
			}

			var listHTML   = '<li><a href="/detail/jenre.html?book'+ data[i]['id'] +'?' + category + '"><img src="' + data[i]['image'] +'" alt=""></a></li>';

			if( categoryURL === category ){
				$listIndex.append(listHTML);
			}
		});

		for( var i=1; i<=$listIndex.find('li').length; i++ ){
			num.push(i);
		}

		Array.prototype.shuffle = function() {
			var i = this.length;
			while(i){
				var j = Math.floor(Math.random()*i);
				var t = this[--i];
				this[i] = this[j];
				this[j] = t;
			}
			return this;
		}
		numShuffle = num.shuffle();

		// 100冊一覧の本をランダム配置
		$listIndex.find('li').each(function(i){
			$(this).addClass('item-' + numShuffle[i] );
		});
	}

	var getJSONData = function(path,type){
		var dfd=$.Deferred();
		$.ajax({
			cache:false,
			url:path,
			type:type,
			data:{},
			dataType:"json",
			success:function(data){
				dfd.resolve(data);
			},
			error:function(data){
				dfd.reject(data);
			}
		});
		return dfd.promise();
	}

	$(window).load(function(){
		getJSONData('/api/s_book100.json', 'GET').done(
			function(data){
				setDOM(data);
			}
		);
	});
});