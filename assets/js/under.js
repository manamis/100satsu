
$(function(){
	
// ----------------------------------------------------------------------------
// Sound Check
// ----------------------------------------------------------------------------	
	var SoundCheck = function(){

		var options = {
			easing   : 'easeInQuad',
			interval : 5000
		};	
		
		
		var $container    = $('#container'),
			$header       = $('#header'),
			$content      = $('#content'),
			$footer       = $('#footer'),
			$qunta        = $('#qunta'),
			$sound        = $('.sound-select-under'),
			$soundOn      = $sound.find('.on'),
			$soundOff     = $sound.find('.off'),
			soundFlg      = false;
			firstSoundFlg = false;
	
	
		var init = function(){
			$('html,body').animate({ scrollTop: 0 }, '100', function(){
				//$content.hide();
			});
			$('.sound-select-under, .enter, #header, #content, #footer, #qunta').css({ opacity : 0 });
			
			imgPreload();
		}
		
		
		var imgPreload = function(){
			$container.imagesLoaded(function(){
				$('body').css('background-image', 'url(/assets/img/bg/bg_body.png)');
				$header.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
				$content.css({ visibility : 'visible', display : 'block' }).stop().animate({ 'opacity' : 1 },600, 'linear');
				$qunta.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
				$footer.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
				$('#nav-pagetop, #nav-pagetop-sp').css({ opacity : 1 });
				$.cookie("shincho-2015-access", 'access', { expires: date, path: '/' });
				//eventSet();
			});
		}
		
		var eventSet = function(){
			$soundOn.on('click', function(){ $soundOnClick($(this)) });
			$soundOff.on('click', function(){ $soundOffClick($(this)) });
		}

		var $soundOnClick = function($elm){
			$sound.stop().fadeOut(500, 'linear');
			soundFlg = true;
			firstSoundFlg = false;
			$('.sound-check').addClass('on');
			$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-on_02.png');
			document.getElementById('bgm-sound').play();
			SoundStart();
			$('body').css('background-image', 'url(/assets/img/bg/bg_body.png)');
			$header.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
			$content.css({ visibility : 'visible', display : 'block' }).stop().animate({ 'opacity' : 1 },600, 'linear');
			$qunta.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
			$footer.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
			$('#nav-pagetop, #nav-pagetop-sp').css({ opacity : 1 });
			$.cookie('shincho-sound', 'sound', { expires: date, path: '/' });
			$.cookie("shincho-2015-access", 'access', { expires: date, path: '/' });
		}
		
		
		var $soundOffClick = function($elm){
			$sound.stop().fadeOut(500, 'linear');
			soundFlg = false;
			$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-off_02.png');
			SoundOFFCheck();
			$('body').css('background-image', 'url(/assets/img/bg/bg_body.png)');
			$header.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
			$content.css({ visibility : 'visible', display : 'block' }).stop().animate({ 'opacity' : 1 },600, 'linear');
			$qunta.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
			$footer.css({ visibility : 'visible' }).stop().animate({ 'opacity' : 1 },600, 'linear');
			$('#nav-pagetop, #nav-pagetop-sp').css({ opacity : 1 });
			$.cookie("shincho-2015-access", 'access', { expires: date, path: '/' });
		}
			
		init();	
	}


	SoundStart = function(){
		var $soundCheck  = $('.sound-check'),
			$soundClick  = $('.sound-click'),
			bgm           = document.getElementById('bgm-sound'),
			clickSound    = document.getElementById('click-sound'),
			checkflg      = true;
			ClickSoundFlg = true;
		
		$soundCheck.on('click', function(){ $soundCheckClick($(this)) });
		
		var $soundCheckClick = function($elm){
			
			if( checkflg === true ){
				checkflg = false;
				firstSoundFlg = false;
				
				if( $elm.hasClass('on') ){
					bgm.pause();
					//bgm.currentTime = 0;
					ClickSoundFlg = false;
					$elm.find('img').attr('src', '/assets/img/btn/btn-sound-off_02.png');
					$elm.removeClass('on');
					$.removeCookie('shincho-sound', { expires: date, path: '/'});
				} else{
					bgm.play();
					clickSound.play();
					ClickSoundFlg = true;
					$elm.addClass('on');
					$.cookie('shincho-sound', 'sound', { expires: date, path: '/' });
					$elm.find('img').attr('src', '/assets/img/btn/btn-sound-on_02.png');
				}
				
				setTimeout(function(){	
					checkflg = true;
				}, 300);
			}
		}

		$soundClick.on('click', function(e){
			if( ClickSoundFlg === true ){
				
				if( ua.uaObj.uaDevice === 'device-mobile' ){
					clickSound.pause();
					//clickSound.currentTime = 0;
					clickSound.play();
				} else{
					e.preventDefault();
					
					var href = $(this).attr('href');
					
					clickSound.pause();
					//clickSound.currentTime = 0;
					clickSound.play();
					
					if( href ){
						if( $(this).hasClass('window-open') ){
							setTimeout(function(){	
								window.open().location.href = href;
							}, 1000);
						} else if( $(this).hasClass('no-window') ){
							return false;
						} else{
							setTimeout(function(){	
								location.href = href;
							}, 1000);
						}
					}
				}
			}
		});
	}


	SoundOFFCheck = function(){
		$('.sound-check').on('click', function(){
			if( firstSoundFlg === true ){
				firstSoundFlg = false;
				$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-on_02.png');
				$('.sound-check').addClass('on');
				SoundStart();
				document.getElementById('bgm-sound').play();
				document.getElementById('click-sound').play();
				$.cookie('shincho-sound', 'sound', { expires: date, path: '/' });	
			}
		});		
	}


	if( !$.cookie("shincho-2015-access") && ua.uaObj.uaBrouser !== 'ie8' ){
		SoundCheck();
	} else{
		$('.sound-select-under').hide();
		$('body').css('background-image', 'url(/assets/img/bg/bg_body.png)');
		$('#header').css({ visibility : 'visible', display : 'block' });
		$('#content').css({ visibility : 'visible' });
		$('#footer').css({ visibility : 'visible' });
		$('#qunta').css({ visibility : 'visible' });
		$('#nav-pagetop, #nav-pagetop-sp').css({ opacity : 1 });
		
		if( ua.uaObj.uaBrouser !== 'ie8' ){
			if( $.cookie('shincho-sound') ){
				$('.sound-check').addClass('on');
				$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-on_02.png');
				document.getElementById('bgm-sound').play();
				SoundStart();
			} else{
				var soundCheckFirstFlag = true;
				$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-off_02.png');
				$('.sound-check').on('click', function(){
					if( soundCheckFirstFlag === true ){
						soundCheckFirstFlag = false;
						$('.sound-check').addClass('on');
						$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-on_02.png');
						document.getElementById('bgm-sound').play();
						SoundStart();
						$.cookie('shincho-sound', 'sound', { expires: date, path: '/' });	
					}
				});
			}
		}
	}
	
	if( ua.uaObj.uaBrouser === 'ie8' ){
		$('.sound-check').hide();
		$.cookie('shincho-sound', 'sound', { expires: date, path: '/' });
		$.cookie("shincho-2015-access", 'access', { expires: date, path: '/' });
	}
});
