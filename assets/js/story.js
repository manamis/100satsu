$(function(){
	
// ----------------------------------------------------------------------------
// Story JS
// ----------------------------------------------------------------------------
	var posNum            = 1;

	var slideShow = function(slideshow){
		var $slideshow = $(slideshow),
			$picList    = $slideshow.find('.pic-list'),
			$picListLen = $picList.find('li').length;
			$prev       = $slideshow.find('.prev'),
			$next       = $slideshow.find('.next'),
			clickFlag   = true;
				
		$picList.find('li').css({ 'opacity' : '0', 'z-index' : 0 });
		$picList.find('.current').css({ 'opacity' : '1', 'z-index' : 1 });
		
		$next.on('click', function(){
			if( clickFlag === true ){
				clickFlag = false;
				
				if( ($picListLen) != (posNum) ){
					var $nextLi = $picList.find('.current').next();
					
					$nextLi.stop().animate({ 'opacity' : 1 },200, 'swing');
					
					$prev.show();
					
					posNum++;
										
					if( ($picListLen) === (posNum) ){
						$next.hide();
					} else{
						$next.show();
					}
					
					$picList.find('.current').stop().animate({ opacity : 0 }, 800, 'swing', function(){
						$picList.find('.current').css({ 'z-index' : 0 }).removeClass('current');
						$nextLi.css({ 'z-index' : 1 }).addClass('current');
						
						setTimeout(function(){	
							clickFlag = true;
						}, 100);
					});						
				}
			}
		});
		
		$prev.on('click', function(){
			if( clickFlag === true ){
				clickFlag = false;
				
				if( posNum != 1){
					var $prevLi = $picList.find('.current').prev();
					
					$prevLi.stop().animate({ 'opacity' : 1 },200, 'swing');
					
					$next.show();
					
					posNum--;
					
					if( (posNum) === 1 ){
						$prev.hide();
					} else{
						$prev.show();
					}
					
					$picList.find('.current').stop().animate({ opacity : 0 }, 800, 'swing', function(){
						$picList.find('.current').css({ 'z-index' : 0 }).removeClass('current');
						$prevLi.css({ 'z-index' : 1 }).addClass('current');
						
						setTimeout(function(){	
							clickFlag = true;
						}, 100);
					});			
				}
			}
		});
	}
		
		
	var ModalWindow = function(){
		var $modalBtn   = $('.modal'),
			$window     = $(window),
			category    = '';
		
		
		var setPos = function(mTop){
			var $windowHeight = $window.height(),
				$boxHeight    = $('.modal-' + category).height();
			
			$('.overray').height($(window).height());
				
			if( $windowHeight > $boxHeight ){
				$('.modal-' + category).css({ top : ($windowHeight - $boxHeight)/2 });
			}
		}
		
		$window.resize(function(){	
			if( ua.uaObj.uaDevice !== 'device-mobile' ){
				setPos();
			}
		});
		
		
		var init = function(){
			category = location.href.split('?')[1];
			
			$('.modal-' + category).imagesLoaded(function(){
				setPos();
			});
			
			$('.overray-' + category).height($(window).height()).fadeIn(800, 'swing');	

			$('.modal-' + category).fadeIn(1000, 'swing', function(){
				if( window.navigator.appVersion.toLowerCase().indexOf("msie 8.") != -1 ){
					this.style.removeAttribute("filter");
				}
			});
			
			$('.modal-' + category).find('.close').on('click',function(){
				location.href = '/story/';
			});
			
			slideShow('.modal-' + category);			
		}
		
		init();
	};


	if( !$.cookie("shincho-2015-access") ){
		location.href = '/story/';
		
	} else{
		ModalWindow();
	}
});