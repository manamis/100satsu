/**
 * YoutubeEventTracking
 *DOM：<div class="youtube-player" data-src="https://www.youtube.com/embed/gkvx5SWD9ks?rel=0"></div>
 */
var YoutubeEventTracking = (function() {
    function YoutubeEventTracking(element) {
        this.el = element;
        if (this.el) {
            this.init();
        }
    }

    YoutubeEventTracking.prototype = {
        init: function() {
            this.checkReady();
        },
        checkReady: function() {
            if (onYouTubeIframeAPIReady.ready) {
                this.setupYoutube();
            } else {
                setTimeout(this.bind(this.checkReady, this), 300);
            }
        },
        setupYoutube: function() {

            var videoSrc = $(this.el).data('src').match(/.*embed\/([\d\w-]+)(.*)/);
            var params = videoSrc[2] ? videoSrc[2].slice(1).split('&') : '';
            var playerVars = {};
            var settings = {};

            for (i = 0; i < params.length; i++) {
                playerVars[params[i].split('=')[0]] = params[i].split('=')[1];
            }

            settings = {
                width: $(this.el).attr('width'),
                height: $(this.el).attr('height'),
                videoId: videoSrc[1],
                playerVars: playerVars,
                events: {
                    'onStateChange': onPlayerStateChange,
					'onReady': onPlayerReady
                }
            };
            return new YT.Player(this.el, settings);
        },
        bind: function(func, obj) {
            var nativeBind = Function.prototype.bind,
                args;
            if (nativeBind && func.bind === nativeBind) {
                return nativeBind.apply(func, [].slice.call(arguments, 1));
            }
            args = [].slice.call(arguments, 2);
            return function() {
                return func.apply(obj, args.concat([].slice.call(arguments)));
            };
        }
    };

    return YoutubeEventTracking;
}());

/**
 * YoutubeIframeAPIの関数。APIが実行可能になったら呼ばれる。
 */
function onYouTubeIframeAPIReady() {
    onYouTubeIframeAPIReady.ready = true;
}

function onPlayerReady(event) {
}


/**
 * YoutubeIframeAPIの関数。Youtubeのステータスが変更されたら呼ばれる。
 */
function onPlayerStateChange(event) {
	var bgm        = document.getElementById('bgm-sound'),
		clickSound = document.getElementById('click-sound');

    if (event.data == YT.PlayerState.ENDED) {
    	$(function() {
			if( $.cookie('shincho-sound') ){
				bgm.play();
				ClickSoundFlg = true;
				$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-on_02.png');
				$('.sound-check').addClass('on');
			}
		});
    }

    if (event.data == YT.PlayerState.PLAYING) {
    	$(function() {
			bgm.pause();
			bgm.currentTime = 0;
			ClickSoundFlg = false;
			$('.sound-check').find('img').attr('src', '/assets/img/btn/btn-sound-off_02.png');
			$('.sound-check').removeClass('on');
		});
    }
}


$(function(){
    var tag = document.createElement('script');
    var firstScriptTag = document.getElementsByTagName('script')[0];
    tag.src = "https://www.youtube.com/iframe_api";
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    $(function() {
		var setDOM = function(data){
			var categoryAlt  = '',
				categoryClass = '';

			switch ( data['results']['category_id'] ){
				case '1' :
					categoryAlt   = '泣ける本';
					categoryClass = 'cry';
				break;

				case '2' :
					categoryAlt   = '考える本';
					categoryClass = 'think';
				break;

				case '3' :
					categoryAlt   = 'ヤバイ本';
					categoryClass = 'dangerous';
				break;

				case '4' :
					categoryAlt   = 'シビれる本';
					categoryClass = 'becomenumb';
				break;

				case '5' :
					categoryAlt   = '恋する本';
					categoryClass = 'love';
				break;
			}

			$('.youtube-player').attr('data-src', '//www.youtube.com/embed/' + data['results']['movie_tag'] + '?&showinfo=0&rel=0&wmode=transparent');

			$('.fb-link').attr('href', 'http://www.facebook.com/share.php?u=https://www.youtube.com/watch?v=' + data['results']['movie_tag']);
			$('.tw-link').attr('href', 'http://twitter.com/share?text=%e3%83%8a%e3%83%ac%e3%83%bc%e3%82%bf%e3%83%bc%e3%81%af%e3%81%84%e3%81%be%e8%a9%b1%e9%a1%8c%e3%81%ae%e3%83%94%e3%83%bc%e3%82%b9%e5%8f%88%e5%90%89%e3%81%95%e3%82%93%ef%bc%81%ef%bc%81%e6%96%b0%e6%bd%ae%e6%96%87%e5%ba%ab%e3%81%ae%e6%96%b0%e3%81%97%e3%81%84%e9%a1%94%e3%80%81%e3%80%8cQUNTA%ef%bc%88%e3%82%ad%e3%83%a5%e3%83%b3%e3%82%bf%ef%bc%89%e3%80%8d%e3%81%8c%e3%80%81%e8%a6%8b%e3%82%8b%e3%81%9f%e3%81%b3%e3%81%82%e3%81%9f%e3%82%89%e3%81%97%e3%81%84%e6%84%9f%e6%83%85%e3%81%ab%e5%87%ba%e4%bc%9a%e3%81%84%e3%81%be%e3%81%99%e3%80%82&url=https://www.youtube.com/watch?v=' + data['results']['movie_tag']);


			if( data['results']['category_id'] == 0){
				$('.nav-btn-03').hide();
			} else{
				$('.nav-btn-03').find('a').attr('href', '/jenre/list.html?' + categoryClass );
				$('.nav-btn-03').find('img').attr('src', '/assets/img/btn/btn_jenre_0' + data['results']['category_id'] + '.png').attr('alt', categoryAlt + '一覧を見る');
			}

			new YoutubeEventTracking($('.youtube-player')[0]);

		}


		var getJSONData = function(path,type){
			var dfd=$.Deferred();
			$.ajax({
				cache:false,
				url:path,
				type:type,
				data:{},
				dataType:"json",
				success:function(data){
					dfd.resolve(data);
				},
				error:function(data){
					dfd.reject(data);
				}
			});
			return dfd.promise();
		}

		$(window).load(function(){
			var tagID = location.href.split('?')[1];

			if(! tagID ){
				tagID = '';
			}

/*			getJSONData('/api/get_movie_tag.php?' + tagID, 'GET').done(*/
		getJSONData('/api/movie_tag.json', 'GET').done(
				function(data){
					setDOM(data);
				}
			);
		});
    });
});