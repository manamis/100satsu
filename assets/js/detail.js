$(function(){

// ----------------------------------------------------------------------------
// Roll Over
// ----------------------------------------------------------------------------
	var setRollOver = function(){
		var roll   = 'roll',
			unroll = 'unroll';

		function find_child(elm){

			var collect_img = {},
				collect_a = {},
				counter_img = 0,
				counter_a = 0;

			function get_elms(elm){
				var element = elm;

				elm.children().each(function(){

					if($(this).attr('class') !== unroll){
						if(this.tagName === 'IMG' || this.tagName === 'INPUT'){
							collect_img[counter_img] = $(this);
							counter_img++;
						}else if(this.tagName === 'A'){
							var tag_a = $(this);
							tag_a.find('img').each(function(){
								if($(this).attr('class') !== unroll){
									collect_img[counter_img] = $(this);
									counter_img++;
								}
							});

							collect_a[counter_a] = tag_a;
							counter_a++;

							return true;
						}
						get_elms($(this));
					}else{
						return true;
					}
				});
			}

			get_elms(elm);

			return [collect_img,collect_a];
		}

		$('.' + roll).each(function(){
			var items = find_child($(this)),
				items_img = items[0],
				items_link = items[1],
				onmouse_check = false;
				btnParent = $(this).parent();

			$.each(items_img, function(i){

				var btn = items_img[i];

				if(btn[0].tagName === 'INPUT' && btn.attr('type') === 'text'){
					return true;
				}

				var bc = btnParent.attr('class');
				if(!bc || bc.indexOf('rollParent') == -1){
					btnParent = btn;
				}else{

					if(!items_link[0]) return;

					btnParent.children('.text').click(function(){
						location.href = items_link[0].attr('href');
					}).css('cursor','pointer');
				}

				btnParent.mouseover(function(){
					if (!onmouse_check) {
						onmouse_check = true;
						btn.attr('src', btn.attr('src').replace(/(\.[^\.]+$)/, "_o$1"));

					}
				});
				btnParent.mouseout(function(){
					btn.attr('src', btn.attr('src').replace(/_o/, ""));
					onmouse_check = false;
				});
			});

			$.each(items_link, function(i){
				var btn = items_link[i],
					imgs = btn.find('img');

				var src_on = imgs.attr('src').replace(/(\.[^\.]+$)/, "_o$1");
				$('<img />').attr('src', src_on);
			});
		});
	};

// ----------------------------------------------------------------------------
// Detail JS
// ----------------------------------------------------------------------------
	var id = location.href.split('?')[1];

	var eventSet = function(){
		var $next      = $('.next'),
			$prev       = $('.prev'),
			bookPrevFlg = true,
			bookNextFlg = true;

		$('.disabled').on('click', function(e){
			e.preventDefault();
			return false;
		});

		$prev.on('click', function(){
			if( bookPrevFlg == true ){
				bookPrevFlg = false,
				detailClassName = '.lyt-detail-01';

				if( $(this).closest(detailClassName).prev()['length'] > 0 ){
					$(this).closest(detailClassName).fadeOut( 500,'swing', function(){

						$('#qunta').find('img').attr('src', '/assets/img/img/img_qunta_01_' + $(this).closest(detailClassName).prev().attr('data-jenre') + '.gif');
						$('#footer').removeClass().addClass($(this).closest(detailClassName).prev().attr('data-jenre'));

						$(this).closest(detailClassName).prev().fadeIn(500, 'easeInCubic', function(){
							if( $(this).closest(detailClassName).prev()['length'] == 0 ){
								 $(this).find('.prev').hide();
							}

							if ( ua.uaObj.uaBrouser !== 'ie8' && ua.uaObj.uaBrouser !== 'ie9' ){
								history.pushState(null, null,'?' + $(this).closest(detailClassName).attr('id') + '?' + $(this).closest(detailClassName).attr('data-jenre') );
							}
						});
					});

					setTimeout(function(){
						bookPrevFlg = true;
					}, 1500);
				}
			}
		});

		$next.on('click', function(){
			if( bookNextFlg == true ){
				bookNextFlg = false,
				detailClassName = '.lyt-detail-01';

				if( $(this).closest(detailClassName).next()['length'] > 0 ){
					$(this).closest(detailClassName).fadeOut( 500,'swing', function(){

						$('#qunta').find('img').attr('src', '/assets/img/img/img_qunta_01_' + $(this).closest(detailClassName).next().attr('data-jenre') + '.gif');
						$('#footer').removeClass().addClass($(this).closest(detailClassName).next().attr('data-jenre'));

						$(this).closest(detailClassName).next().fadeIn(500, 'easeInCubic', function(){
							if( $(this).closest(detailClassName).next()['length'] == 0 ){
								 $(this).find('.next').hide();
							}

							if ( ua.uaObj.uaBrouser !== 'ie8' && ua.uaObj.uaBrouser !== 'ie9' ){
								history.pushState(null, null,'?' + $(this).closest(detailClassName).attr('id') + '?' + $(this).closest(detailClassName).attr('data-jenre') );
							}
						});
					});

					setTimeout(function(){
						bookNextFlg = true;
					}, 1500);

				} else{
					bookNextFlg = true;
				}
			}
		});

		/*$('.lyt-detail-01').touchwipe({
			wipeLeft: function(){
				$next.click();
			},

			wipeRight: function(){
				$prev.click();
			},

			preventDefaultEvents: false
		});*/
	}


	var bookShow = function(){
		$('#' + id).fadeIn(1000);
	}


	var nextPrevCheck = function(){
		if ( $('.lyt-detail-01-container').find('#' + id).next()['length'] == 0 ){
			$('.lyt-detail-01-container').find('#' + id).find('.next').hide();
		}

		if ( $('.lyt-detail-01-container').find('#' + id).prev()['length'] == 0 ){
			$('.lyt-detail-01-container').find('#' + id).find('.prev').hide();
		}
	}


	var imgPreload = function(){
		$('#container').imagesLoaded(function(){
			eventSet();
			bookShow();
			nextPrevCheck();

			if( ua.uaObj.uaDevice !== 'device-mobile' ){
				setRollOver();
			}
		});
	}


	var setDOM = function(data){
		var $detailContainer = $('.lyt-detail-01-container'),
			categoryAlt      = '',
			categoryClass    = '',
			jenre            = location.href.split('?')[2];

		$('#qunta').find('img').attr('src', '/assets/img/img/img_qunta_01_' + jenre + '.gif').css({ visibility : 'visible' });

		setTimeout(function(){
			$('#footer').addClass(jenre).css({ visibility : 'visible' });
		}, 600);

		$.each( data, function(i){

			switch ( data[i]['category'] ){
				case '泣ける本' :
					categoryAlt   = '泣ける本';
					categoryClass = 'cry';
				break;
				case 'ヤバイ本' :
					categoryAlt   = 'ヤバイ本';
					categoryClass = 'dangerous';
				break;
				case '恋する本' :
					categoryAlt   = '恋する本';
					categoryClass = 'love';
				break;
				case '考える本' :
					categoryAlt   = '考える本';
					categoryClass = 'think';
				break;
				case 'シビれる本' :
					categoryAlt   = 'シビれる本';
					categoryClass = 'becomenumb';
				break;
			}

			if( data[i]['url_detail'] == '' ){
				urlDetail = 'disabled';
				imgPath   = '_disabled';
			} else{
				urlDetail = 'roll';
				imgPath   = '';
			}

			var detailHTML = '<div id="book' + data[i]['id'] + '" class="lyt-detail-01" data-jenre="' + categoryClass + '"><p class="prev roll"><img src="/assets/img/btn/btn_prev_01.png" alt="前へ"></p><p class="next roll"><img src="/assets/img/btn/btn_next_01.png" alt="次へ"></p><h2 class="hdg-level2-02"><img src="/assets/img/hdg/hdg_' + categoryClass + '_01.png" alt="' + categoryAlt + '"></h2><p class="book-image"><img src="'+ data[i]['image_detail'] +'" alt=""></p><div class="lyt-detail-01-inner"><h3 class="title ' + categoryClass + '">' + data[i]['header'] + '</h3><p class="description">' + data[i]['body'] + '</p><ul class="data-list"><li><dl><dt><img src="/assets/img/hdg/hdg_detail_01.png" alt="作品名"></dt><dd>' + data[i]['title'] + '</dd></dl></li><li class="w-half"><dl><dt><img src="/assets/img/hdg/hdg_detail_02.png" alt="著者"></dt><dd>' + data[i]['auther'] + '</dd></dl></li><li class="w-half"><dl><dt><img src="/assets/img/hdg/hdg_detail_03.png" alt="定価"></dt><dd>' + data[i]['price'] + '円</dd></dl></li></ul><ul class="btn-list"><li class="first"><a href="'+ data[i]['url_detail'] +'" target="_blank" class="'+ urlDetail +'"><img src="/assets/img/btn/btn_browse_01'+ imgPath +'.png" alt="立ち読み"></a></li><li><a href="' + data[i]['url_buy'] + '" target="_blank" class="roll"><img src="/assets/img/btn/btn_buy_02.png" alt="購入する"></a></li></ul><p class="nav-back roll"><a href="/jenre/list.html?'+ categoryClass +'"><img src="/assets/img/btn/btn_back_01_' + categoryClass + '.png" alt="' + categoryAlt + '一覧"></a></p></div></div>';

			$detailContainer.append(detailHTML);

		});

		imgPreload();
	}


	var getJSONData = function(path,type){
		var dfd=$.Deferred();
		$.ajax({
			cache:false,
			url:path,
			type:type,
			data:{},
			dataType:"json",
			success:function(data){
				dfd.resolve(data);
			},
			error:function(data){
				dfd.reject(data);
			}
		});
		return dfd.promise();
	}

	$(window).load(function(){
/*
		getJSONData('/api/s_book100.php', 'GET').done(
*/
	getJSONData('/api/s_book100.json', 'GET').done(
			function(data){
				setDOM(data);
			}
		);
	});
});