/*

<img class="data-img" src="http://placehold.it/1x1" data-sml="common/img/main.png" data-med="common/img/main.png" data-lrg="common/img/main.png" alt="">

*/

$.fn.dataImg=function(a){function b(a){var b=$(window).width();return a.data(b>d.med?"lrg":b<=d.med&&b>d.sml?"med":"sml")}function c(){e.each(function(){var a=$(this),c=b(a);void 0!=c&&(a.is("img")?a.attr("src",c):a.css("background-image","url("+c+")"))})}var d=$.extend({sml:400,med:800,lrg:1e3,resize:!1},a),e=$(this);c(),1==d.resize&&$(window).resize(function(){c()})};

  $(function(){
    $('.data-img').dataImg({
      sml: 480,
      med: 1024,
      lrg: 1099,
      resize: true,
      bgImg: true
    });
  });